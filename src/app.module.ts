import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PusherService } from './pusher/pusher.service';
import { PusherController } from './pusher/pusher.controller';
import { PusherModule } from './pusher/pusher.module';

@Module({
  imports: [PusherModule],
  controllers: [AppController, PusherController],
  providers: [AppService, PusherService],
})
export class AppModule {}
