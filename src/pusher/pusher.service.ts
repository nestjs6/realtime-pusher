import { Injectable } from '@nestjs/common';
import * as Pusher from 'pusher';

@Injectable()
export class PusherService {
  private pusher: Pusher;
  constructor() {
    this.pusher = new Pusher({
      appId: '1466529',
      key: 'f7055d0da12d4dafc051',
      secret: 'a99a0bbb7ad80ea6c328',
      cluster: 'us2',
      useTLS: true,
    });
  }

  async trigger(channel: string, event: string, data: any) {
    await this.pusher.trigger(channel, event, data);
  }
}
